import {Component} from 'angular2/core';
import {Video} from './video';
import {PlaylistDetailComponent} from './playlist-detail.component';

@Component({
	selector: 'playlist',
	templateUrl: 'app/ts/playlist.component.html',
	  directives: [PlaylistDetailComponent],
	inputs: ['videosa']
})

export class PlaylistComponent {
	selectedVideo : Video;
	onSelect(vi: Video){
		this.selectedVideo = vi;	
	}
}