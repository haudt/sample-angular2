import {Component} from 'angular2/core';
import {OnInit} from 'angular2/core';
import {Config} from './config.service';
import {Video} from './video';
import {PlaylistComponent} from './playlist.component';
import {PlaylistDetailComponent} from './playlist-detail.component';
import {VideoService} from './video.service';

@Component({
    selector: 'my-app',
    templateUrl: 'app/ts/app.component.html',
     directives: [PlaylistComponent,PlaylistDetailComponent],
      providers: [VideoService]

})

export class AppComponent  {
	mainHeading = Config.MAIN_HEADING;
	  videos: Video[];


  constructor(private videoService: VideoService) { }

  getVideos(): void {
    this.videoService.getVideos().then((videos: Video[]) => this.videos = videos);
  }

  ngOnInit(): any {
    this.getVideos();
  }



 
}
